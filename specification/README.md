[[_TOC_]]

# Introduction
## Version 1
[Version 1](specification/version1/README.md) (~June 2020) of the specification was driven by discussions in recent years. The intention was to describe a simple REST interface in openAPI that was based on the data structure of Map of Tomorrow. The coordination of this interface was done between WeChange (Ludwig, Simon), Karte von Morgen (Helmut, Markus) and fairsync (Roland/Fredy).
With this approach, however, the goals of the fairsync project agreed with NLnet were not really achieved.

## On the way to Version 2
Fredy did a lot of research in the coming months, talked to many people and played with https://solidproject.org. 
Around December 2020 he came to the conclusion that https://solidproject.org was not yet suitable for the exchange of organizational data, because especially searching data within a pod as well as across pods was not yet available. So the idea of each organization storing its data in its own pod was dead for the time being. 
Around the same time, Fredy came across the https://ec.europa.eu/isa2 project, which defined a data model that was very close to the data model defined in version 1. 

The conclusion from this experience 
1. data should be available in RDF (Resource Description Framework). Prefered format is turtle.
2. as far as possible compatible to https://ec.europa.eu/isa2
3. a triple store should be used that provide an SPARQL endpoint.
4. it should be possible later to query data from multiple triple stores. (Assumption/hope: https://solidproject.org will eventually support SPARQL and pods can then be included as triple stores).

## Version 2
With [Version 2](specification/version2/domain_ISA2.md) we hope to have found a way that we are satisfied with in the long term, that is expandable and flexible an interoperable with a lot of systems. We also provide new so called ['Linked Open Data'](https://lod-cloud.net/) and are sure to have found a way that will become more and more popular.

### Karte von Morgen Adapter
To fullfill the requirements of Version 2 we decide to use [rdf4j](https://rdf4j.org). They provide a nice workbench and a powerfull server to manage Repositories (Triple Stores), SPARQL Endpoints, SPQRQL Proxy Repository and federated Repositories.

The instance of our prototype can be found here https://rdf.dev.osalliance.com/rdf4j-workbench

To provide the Karte von Morgen (KVM) Data as RDF fulfill the Version 2 Specification we provide a [kvm-adapter](https://git.fairkom.net/fairsync/development/kvm-adapter) that is able to do a initial load from all KVM Organisations to our rdf4j instance and also a scheduled job, that keeps the rdf4j instance in sync with the KVM Data. (As of January 9, 2021, we are still working on the prototype)

## Version 3
"Aller guten Dinge sind drei" ;-) -> "All good things come in threes"  
Since Mai 2021 the current doku is here: https://linkedopenactors.org/
