# Topic dubletten

Ich sehe das so: 
* Es gibt viele dubletten checker, da jeder sein ego hat und keiner anderen vertraut.
* Es gibt viele Data Owner, z.B. kvm, weChange, etc.
* A Jeder Data owner SOLLTE/MUSS eine Schnittstelle "sameAsProposal" anbieten
* B Jeder Data owner SOLLTE/MUSS eine Schnittstelle "differentFromProposal" anbieten
* C Jeder Data owner KANN eine Schnittstelle "sameAs" anbieten
* D Jeder Data owner KANN eine Schnittstelle "differentFrom" anbieten
* E Jeder Data owner SOLLTE/MUSS eine Schnittstelle zum lesen von Orgas anbieten, die sameAs und differentFrom bereitstellt. (In unserem Format in Turtle, optional andere RDF Formate) [Zu definieren, ob json+ld besser wäre wegen AP]
* Ein data owner KANN einem Actor (Application, Group, Organization, Person, Service) Berechtigung geben C und/oder D aufzurufen.  
    * dubletten checker ist also indirect ein Agent!!  
* Ein data owner SOLLTE/MUSS A und B öffentlich bereitstellen. 
* Ein data owner SOLLTE/MUSS einen individuelle internen prozess für A und B erarbeiten


