
##########################################  
##########################################  

**Deprecated !!!** use https://linkedopenactors.org/

##########################################  
##########################################  

# Einleitung
Neben der Interessen der Wirtschaft etabliert sich eine verteilte Gemeinschaft von Menschen, die nicht an ein endloses Wachstum glaubt und versucht durch ein anderes Handeln einen sozialökologischen Wandel der Gesellschaft zu gestalten. Siehe auch [Memorandum of Understanding des Wandelbündnis](https://wandelbuendnis.org/mou/memorandum-of-understanding).

Um diesen Wandel sichtbar zu machen, ist es nötig die Organisationen des sozialökologischen Wandel zu vernetzen. Dazu werden Technologien verwendet wie das [Resource Description Framework](https://de.wikipedia.org/wiki/Resource_Description_Framework) welche den Ansatz der [Linked_Open_Data](https://wikipedia.org/wiki/Linked_Open_Data) (**LOD**) ermöglichen.

Das hier werwendete Vokabular orientiert sich am [ISA2 core vocabularies](https://ec.europa.eu/isa2/sites/isa/files/corevocabularies-poster.pdf). Es wird jedoch Abweichungen geben müssen. Z.B. gibt es im [ISA2 core vocabularies](https://ec.europa.eu/isa2/sites/isa/files/corevocabularies-poster.pdf) als Agent nur eine Person oder eine rechtliche Organisation. Im sozialökologischen Wandel gibt es Gruppierungen die eher der Definitiuon einer [FOAF:Group](http://xmlns.com/foaf/spec/#term_Group) entsprechen.

> Die Klasse "Gruppe" repräsentiert eine Sammlung von einzelnen Agenten (und kann selbst die Rolle eines Agenten spielen, d. h. etwas, das Aktionen ausführen kann).
> 
> Dieses Konzept ist absichtlich recht weit gefasst und umfasst informelle und Ad-hoc-Gruppen, langlebige Gemeinschaften, organisatorische Gruppen innerhalb eines Arbeitsplatzes usw. Einige solcher Gruppen können zugehörige Eigenschaften haben, die in RDF erfasst werden können (vielleicht eine Homepage, ein Name, eine Mailingliste usw.). 
> 

Bekannte Datenquellen des LOD sind [DBpedia](https://de.wikipedia.org/wiki/DBpedia) und [Wikidata](https://de.wikipedia.org/wiki/Wikidata). Es werden jedoch immer mehr, z.B. im öffentlichen Bereich [GOVDATA](https://www.govdata.de).

Es soll kein weiterer zentraler Datenspeicher entstehen, sondern ein Standard der beschreibt wie Daten von Agenten (Personen, Gruppen und Organisationen) des sozialökologischen Wandel präsentiert, gespeichert und ausgetauscht werden können.

Ein weiteres Hauptaugenmerk sind sogenannte Duplikate. Dabei handelt es sich um Agenten, die durch die Verteilung von Daten an mehreren Stellen gespeichert wurden und ggf. voneinander abweichen. Diese Daten technisch abzugleichen ist ein eher komplexes Thema, deshalb begnügen wir uns damit diese Duplikate zu markieren, so das man ihnen 'folgen' kann und nach ihnen suchen kann. Ein 'Datenbesitzer' hat dann die Möglichkeit diese Duplikate automatisch oder manuel aufzulösen.

Da sich der sozialökologische Wandel nicht auf den deutschsprachichen Raum begrenzt, wird die Spezifikation in Englisch verfasst. 
Zur Übersetzung von Texten wird [DeepL](https://www.deepl.com) empfohlen.

# Introduction
A distributed community of people who do not believe in endless growth try to bring about socio-ecological change in society by acting differently.  See also [Memorandum of Understanding of Wandelbündnis](https://wandelbuendnis.org/mou/memorandum-of-understanding).

In order to make this change visible, it is necessary to link the organisations of socio-ecological change. For this purpose, technologies such as the [Resource Description Framework](https://de.wikipedia.org/wiki/Resource_Description_Framework) are used which enable the [Linked_Open_Data](https://wikipedia.org/wiki/Linked_Open_Data) (**LOD**) approach.

The vocabulary used here is based on the [ISA2 core vocabularies](https://ec.europa.eu/isa2/sites/isa/files/corevocabularies-poster.pdf). However, there will have to be changes. For example, in [ISA2 core vocabularies](https://ec.europa.eu/isa2/sites/isa/files/corevocabularies-poster.pdf), an agent is only a person or a legal organisation. In socio-ecological change, there are groupings that correspond more to the definition of a [FOAF:Group](http://xmlns.com/foaf/spec/#term_Group).

> The Group class represents a collection of individual agents (and may itself play the role of a Agent, ie. something that can perform actions).
> This concept is intentionally quite broad, covering informal and ad-hoc groups, long-lived communities, organizational groups within a workplace, etc. Some such groups may have associated characteristics which could be captured in RDF (perhaps a homepage, name, mailing list etc.). 

Well-known data sources of the LOD are [DBpedia](https://de.wikipedia.org/wiki/DBpedia) and [Wikidata](https://de.wikipedia.org/wiki/Wikidata). However, there are more and more, e.g. in the public domain [GOVDATA](https://www.govdata.de).

Another main focus is on so-called duplicates. These are agents that have been stored in several places due to the distribution of data and may differ from each other. Matching this data technically is a rather complex issue, so we are content to mark these duplicates so that they can be 'followed' and searched for. A 'data owner' then has the option of resolving these duplicates automatically or manually.

# Introduction to the concepts

## Short version
- For a generel introduction on What is Linked Data? see [Manu Sporny's Introduction](https://youtu.be/4x_xzT5eF5Q) it's about 12 minutes.
- For the concept of URI, URL, Namespaces and Prefix (in this video called CURIE) see [RDFa Basics](https://youtu.be/ldl0m-5zLz4) from 1:25 to 2:23
    - a longer detailed description about URIs [How to name Things URIs](https://youtu.be/z9NxpKMiLBw) about 20 minutes
- A introduction about RDF [1.7 How to Represent Simple Facts with RDF](https://youtu.be/FaeOtrRsZt4) from 3:16 to 10:02
- The turtle format explained [RDF and Turtle Serialization](https://youtu.be/PADwVsHA7H0) about 14 minutes

## Expert version
- [Knowledge Graphs Prof. Dr. Harald Sack, Dr. Mehwish Alam](https://open.hpi.de/courses/knowledgegraphs2020)
- [Eclipse RDF4J - Working with RDF in Java](https://www.youtube.com/watch?v=OOgXDUf8Cps&t=6s)
- [About RDF4J](https://rdf4j.org/about) we will use [RDF4J Server and Workbench](https://rdf4j.org/documentation/tools/server-workbench/) as Triple Store and SPARQL Endpoint.

# Class Diagram
The colours represent different vocabularies.  
  
![Class Diagram](domain_diagrams/class_diagram.png "Class Diagram")

## Agent
**Typ:**  
[http://xmlns.com/foaf/0.1/Agent](http://xmlns.com/foaf/spec/#term_Agent)   

**Description:**  
Core or Head of our domain is an Agent (eg. person, group, software or physical artifact). 

**Properties:**
- [http://purl.org/dc/terms/identifier](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/identifier)  
Recommended practice is to identify the resource by means of a string conforming to an identification system. Examples include International Standard Book Number (ISBN), Digital Object Identifier (DOI), and Uniform Resource Name (URN). Persistent identifiers should be provided as HTTP URIs.  
**TODO** how to Content_Negotiation ?? in case of kvm or adapters
- [http://xmlns.com/foaf/0.1/name](http://xmlns.com/foaf/spec/#term_name)  
The name of something is a simple textual string.  
- [http://xmlns.com/foaf/0.1/homepage](http://xmlns.com/foaf/spec/#term_homepage)  
A homepage for some thing.   
- [http://www.w3.org/2002/07/owl#versionInfo](https://www.w3.org/TR/owl-ref/#versionInfo-def)  
**NOTE:** _That is not really a good property for that what we need. But i didn't find another yet._
- [http://purl.org/dc/terms/description](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/description)  
Description may include but is not limited to: an abstract, a table of contents, a graphical representation, or a free-text account of the resource.

## Group
**Typ:**  
[http://xmlns.com/foaf/0.1/Group](http://xmlns.com/foaf/spec/#term_Group)

**Description:**  
The Group class represents a collection of individual agents (and may itself play the role of a Agent, ie. something that can perform actions). This concept is intentionally quite broad, covering informal and ad-hoc groups, long-lived communities, organizational groups within a workplace, etc. 

**Properties:**  
- https://schema.org/foundingDate  
The date that this organization was founded.  

## Legal Entity
**Typ:**  
[http://www.w3.org/ns/legal#LegalEntity](https://joinup.ec.europa.eu/site/core_business/rdfs.html#legal:LegalEntity)

**Description:**  
Represents a organisation that is legally registered.

**Properties:**
- https://schema.org/foundingDate  
The date that this organization was founded.  

## Person
**Typ:**  
[http://www.w3.org/ns/person#Person](https://joinup.ec.europa.eu/site/core_person/rdfs.html#person:Person)

**Description:**  
An individual person who may be dead or alive, but not imaginary. It is that restriction that makes Person a sub class of both foaf:Person and schema:Person which both cover imaginary characters as well as real people.

## Organizational Unit
**Typ:**  
[http://www.w3.org/ns/org#OrganizationalUnit](https://www.w3.org/TR/vocab-org/#org:OrganizationalUnit)

**Description:**  
An Organization such as a department or support unit which is part of some larger Organization and only has full recognition within the context of that Organization. In particular the unit would not be regarded as a legal entity in its own right. 

**Properties:**
- https://schema.org/foundingDate  
The date that this organization was founded.  

## Organizational Collaboration
**Typ:**  
[http://www.w3.org/ns/org#OrganizationalCollaboration](https://www.w3.org/TR/vocab-org/#org:OrganizationalCollaboration)

**Description:**  
A collaboration between two or more Organizations such as a project. It meets the criteria for being an Organization in that it has an identity and defining purpose independent of its particular members but is neither a formally recognized legal entity nor a sub-unit within some larger organization. Might typically have a shorter lifetime than the Organizations within it, but not necessarily.

**Properties:**
- https://schema.org/foundingDate  
The date that this organization was founded.  

## Location
**Typ:**  
[http://purl.org/dc/terms/Location](https://joinup.ec.europa.eu/site/core_location/rdfs.html#dcterms:Location)

**Description:**  
Represents any location, irrespective of size or other restriction.
Unlike the [Business Core Vocabulary](https://joinup.ec.europa.eu/site/core_business/rdfs.html) and [The Organization Ontology](https://www.w3.org/TR/vocab-org) models, we use this location for all agents.

See bottom left on the [poster](https://ec.europa.eu/isa2/sites/isa/files/corevocabularies-poster.pdf)

**Properties**
- Geometry (see following headlines)
- Address (see following headlines)

## Geometry
**Typ:**  
[http://www.w3.org/ns/locn#Geometry](https://joinup.ec.europa.eu/site/core_location/rdfs.html#locn:Geometry)

**Description:**  
The Geometry Class provides the means to identify a Location as a point.

**Properties**
- https://schema.org/latitude  
The latitude of a location. For example 37.42242 [(WGS 84)](https://en.wikipedia.org/wiki/World_Geodetic_System).  
- https://schema.org/longitude  
The longitude of a location. For example -122.08585 [(WGS 84)](https://en.wikipedia.org/wiki/World_Geodetic_System).  

## Address
**Typ:**
[http://www.w3.org/ns/locn#Address](https://joinup.ec.europa.eu/site/core_location/rdfs.html#locn:Address)

**Description:**  
An "address representation"

**Properties**
- https://www.w3.org/ns/locn#locn:postCode  
The post code (a.k.a postal code, **zip code** etc.). Post codes are common elements in many countries' postal address systems.  

- https://www.w3.org/ns/locn#locn:adminUnitL1  
The uppermost administrative unit for the address, almost always a **country**.   

- https://www.w3.org/ns/locn#locn:adminUnitL2  
The region of the address, usually a **county**, state or other such area that typically encompasses several localities.  

- https://www.w3.org/ns/locn#locn:postName  
The key postal division of the address, usually the **city**  

- https://www.w3.org/ns/locn#locn:addressArea  
The name or names of a geographic area or locality that groups a number of addressable objects for addressing purposes, without being an administrative unit. This would typically be part of a city, a neighbourhood or village.  
_Note: As a German, I understand this as a street and would map the street including the house number here for Germany._

## Contact Point
**Typ:**  
https://schema.org/ContactPoint

**Description:**  
A contact point for a person or organization.  
Described in the [Core Public Service Vocabulary Application Profile](https://joinup.ec.europa.eu/collection/semantic-interoperability-community-semic/solution/core-public-service-vocabulary-application-profile/releases) see [pdf](
https://github.com/catalogue-of-services-isa/CPSV-AP/raw/master/releases/2.2.1/SC2015DI07446_D02.02_CPSV-AP-2.2.1_v1.00.pdf) chapter    `3.18. The Contact Point Class`
It references [schema.org ContactPoint](https://schema.org/ContactPoint)
  
**Properties**
- https://schema.org/name  
The name of the item. This could be the name of a contact person.
- https://schema.org/email  
Email address.  
- https://schema.org/telephone  
The telephone number.

## Mark as duplicates
https://www.w3.org/TR/owl-ref/#sameAs-def

## Mark as NOT a duplicate
https://www.w3.org/TR/owl-ref/#differentFrom-def

## To be discussed

### Opening OpeningHoursSpecification
In the [Core Public Service Vocabulary Application Profile 2.2.1](https://joinup.ec.europa.eu/collection/semantic-interoperability-community-semic/solution/core-public-service-vocabulary-application-profile/release/221)

The opening Hour specification of schema.org is used. the pdf is available [here](https://github.com/catalogue-of-services-isa/CPSV-AP/raw/master/releases/2.2.1/SC2015DI07446_D02.02_CPSV-AP-2.2.1_v1.00.pdf) See `3.12.5. Opening Hours`.

The OpeningHoursSpecification of [goodrelations](http://www.heppnetz.de/ontologies/goodrelations/v1.html#OpeningHoursSpecification) is also very simmilar to the [schema.org OpeningHoursSpecification](https://schema.org/OpeningHoursSpecification)

The [openstreetmap format](https://wiki.openstreetmap.org/wiki/Key:opening_hours) currently used by karte von morgen differs in format.

TODO: we have to discuss and decide how we solve that. A feew weeks ago we decided to postpone that decision and did not support openingHous in the first version.

### Image
http://xmlns.com/foaf/spec/#term_img
http://xmlns.com/foaf/spec/#term_thumbnail
http://xmlns.com/foaf/spec/#term_logo

### category/tag
https://www.w3.org/TR/vcard-rdf/#d4e813
Bei KVM steckt semantik hinter manchen tags, diese sollte man auflösen!

### License
https://schema.org/license

# Sample Entry
```
@prefix fairsync: <http://fairsync.naturzukunft.de/> .
@prefix person: <http://www.w3.org/ns/person#> .
@prefix schema: <https://schema.org/> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix locn: <http://www.w3.org/ns/locn#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

fairsync:12912983213423098721430ss40569 a foaf:Group;
  dcterms:Location fairsync:12912983213423098721430ss40569_location;
  schema:ContactPoint fairsync:12912983213423098721430ss40569_contactPoint;
  dcterms:identifier "12912983213423098721430ß40569";
  foaf:name "Mutsreman GmbH & Ko KG";
  foaf:homepage "http://example.org";
  owl:versionInfo "3";
  dcterms:description "This is a sample description of this sample organisation!";
  schema:foundingDate "2015-01-02" .

fairsync:12912983213423098721430ss40569_location a dcterms:Location;
  locn:Geometry fairsync:12912983213423098721430ss40569_geometry;
  locn:Address fairsync:12912983213423098721430ss40569_address .

fairsync:12912983213423098721430ss40569_geometry a locn:Geometry;
  schema:latitude 4.669561163368431E1;
  schema:longitude 7.619049236001004E0 .

fairsync:12912983213423098721430ss40569_address a locn:Address;
  locn:postCode "82633";
  locn:adminUnitL1 "Germany";
  locn:adminUnitL2 "BW";
  locn:postName "München";
  locn:addressArea "KluedoStrasse 12" .

fairsync:12912983213423098721430ss40569_contactPoint a schema:ContactPoint;
  schema:name "Mr. Mustermann";
  schema:email "max@mustremann.de";
  schema:telephone "089 62899835" .
```

# Integrating Karte von morgen
## OpenFairDB adapting
![Adapter 1](domain_diagrams/adapter1.png "Adapter 1")

- OpenFairDb has to provide an endpoint for each Resource responding with text/turtle.
- OpenFairDb has to implement a single load of all data to the RDF4J Server (using REST API)
- OpenFairDb has to take care, that the data iin the RDF4J Server is upToDate (using REST API)
- To be clarified: Are there some restrictions for Using SPARQL with that Adapter?
- Sure, it's not possible to use manipulating SPARQL queries. Attention: Can we be sure, that nobody is using it?

## Exteral adapter
![Adapter 2](domain_diagrams/Adapter2.png "Adapter 2")

- external service has to provide an endpoint for each Resource responding with text/turtle.
- external service has to implement a single load of all data to the RDF4J Server (using REST API)
- external service has to take care, that the data iin the RDF4J Server is upToDate (using REST API)
- external service has to communicate with openFairDB to keep syncronized (listening/polling)
- To be clarified: Are there some restrictions for Using SPARQL with that Adapter?
- Sure, it's not possible to use manipulating SPARQL queries. Attention: Can we be sure, that nobody is using it?

## Database replacement
![Database replacement](domain_diagrams/newKvmBackend.png "Database replacement")

- OpenFairDb has to replace it's database with a RDF4J Server (or another SPARQL supporting triple store) 
- Self hosted or RDF4J as a service
