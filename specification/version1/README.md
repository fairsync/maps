FairSync API - Point Of Interests
=================================
FairSync API - Point Of Interests (POI) describes how POIs can be exchanged between maps and platforms. See also https://nlnet.nl/project/FairSync
In the past there was a lot of discussions about this topic and at least as many ideas. In order to make better progress this time, we try to find  
a minimal set of requirements (Minimum Viable Product a.k.a MVP) that provide added value for the initial platforms, which both serve vital communities:

*  https://wechange.de
*  https://kartevonmorgen.org
 
The goal is to start implementation as early as possible and gain experience. With this experience, we then optimize the functionality of the MVP and start analyzing new use cases.

Stakeholder (in alphabetical order)
-----------------------------------
* @naturzukunft (Fredy) as Developer and Architect of FairSync (platform indipendend instance)
* @Helmut as ProductOwnder of kartevonmorgen (KVM)
* Ludwig as ProductOwner of WeChange
* Markus as Developer and Architect of KVM
* @roland.alton as contact person to NLnet foundation at [fairkom](https://fairkom.eu), taking also care on legal issues, open source tools used and overall project focus and controlling


Collection of key points for the MVP
------------------------------------
*  In MVP POI can only be a Organisation (In KVM there are 2 kinds of Organisations called Initiatives and Enterprises).

Use Cases of the MVP
--------------------
* TODO

Componentmodel
--------------
The MVP supports an exchange of data by [nightly-]batch. That means platform A gets all data they are interested in from platform B with one HTTP request.
Each platform is responsible to handle that requested data, that means for MVP each platform has to implement it's own check for duplicates (a.k.a Doubletten-Checker).  
  
![](https://git.fairkom.net/fairsync/maps/-/raw/master/diagrams/FairSync_Componentmodel.png)

ArchitecturModel für Adapter zu cardDAV, iCal, ?Activity?...
---------------------------------------------------
Todo

Technical implementation
------------------------
The data exchange between the platforms is done via [REST](https://de.wikipedia.org/wiki/Representational_State_Transfer).
We descibe the interface(s) using the [OpenAPI Specification (OAS) standard](http://spec.openapis.org/oas/v3.0.3).


### openAPI specification

[fairSyncApi.yml](https://git.fairkom.net/fairsync/maps/-/blob/master/specification/fairSyncApi.yml)

Glossary / Vocabulary
---------------------
| Term | Description |
| ------ | ------ |
| Doubletten-Checker | A peace of code, that do a "check for duplicates" for a set of POIs of a foreign platfrom against your platform |
| platform | For example https://wechange.de | 
| maps | For example a website, that shows POIs in a map, but the website is not a platform. | 
| API | Aplication Programming Interface - a technical specification how data exchange works. In most cases a semantic description |
| POI | Point of Interest - https://wiki.openstreetmap.org/wiki/DE:Points_of_interest |
| MVP | Minimum Viable Product |
| KVM |  Karte von Morgen (https://kartevonmorgen.org) |
| NGI | Next Generation Internet |
| NLnet | NLnet foundation |
| use case | Use cases describe the behavior of a system from a user perspective. |
| user story | we use user story in the same manner as use case |
| journey | we use journey in the same manner as use case |
| Organisation | Organizations are social, relatively stable systems that consist of individuals who share common goals. Organisationen sind soziale, zeitlich relativ stabile Systeme, die aus Individuen bestehen, welche gemeinsame Ziele verfolgen. |
| Nightly Batch | https://de.wikipedia.org/wiki/Stapelverarbeitung / https://en.wikipedia.org/wiki/Batch_processing |

