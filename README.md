This is the [FairSync](https://nlnet.nl/project/FairSync/) module that synchronizes organisations/places between maps and platforms 
  
We have been discussing several approaches how to sync map data from different sources. We realized that the Resource Description Framework (RDF) is a suitable format to exchange data between platforms. And since we are talking about Open Data, it was obvious that we are dealing with Linked Open Data. As ActivityPub is using the term Actor, we specified a Linked Open Actor. That could be an Application, Group, Organization, Person or Service, so an actor is active in a certain context, for example https://wandelbuendnis.org or https://www.w3.org/People/Berners-Lee. We have analysed the formats and needs of several data providers, such as KarteVonMorgen or WECHANGE, who collect data about initiatives and present them on a map or as lists. Furthermore, we have defined in the LOA-specification document:

A LOA Data Provider is any platform providing data about Organisations and Events in LOA format.

A LOA Data Consumer is a platform that consumes Data of a LOA Data Provider in LOA format.

A LOA SPARQL provider is a platform providing a SPARQL endpoint for querying Organisations and Events in LOA format.

A Triple Store Server Provider offers a platform that provides a Triple Store Server like RDF4J. This can be a LOA Data Provider, but also a third party Software as a Service (SaaS) Provider.

Find the full Linked Open Actors specification and an extended overview on LOA here https://linkedopenactors.org

Our demo setup can be reached from the menu of the fairmove.it WordPress page and includes:

Karte von Morgen Map queries possible against https://rdf.dev.osalliance.com/rdf4j-server/repositories/weChange_loa

WECHANGE Map queries possible against https://rdf.dev.osalliance.com/rdf4j-server/repositories/weChange_loa

For manual map data provisioning a CSV uploader has been put online: https://loa.test.opensourceecology.de/ (uploaded LivingLines.at test data including organic farmers Bolsena area Italy, server may not always respond, test-setup!). A documentation for user registration and CSV import has been made available here https://gitlab.com/linkedopenactors/loa-application/-/blob/main/doc/Readme.md

WordPress Plugin is based on http://tools.livinglines.at/ll-tools/wpsparql-doku/
and the configuration can be done via LL-Tools (Admin permissions required)

Find the status of **the specification** [here](specification/README.md).

The development of the LinkedOpenActor server and connectors has been made available with funding from NLnet within the [FairSync](https://nlnet.nl/project/FairSync/) project 2019-10-065.

A separate homepage is available at https://linkedopenactors.org 

